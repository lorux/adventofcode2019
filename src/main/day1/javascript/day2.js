var input = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,6,19,23,2,23,6,27,1,5,27,31,1,31,9,35,2,10,35,39,1,5,39,43,2,43,10,47,1,47,6,51,2,51,6,55,2,55,13,59,2,6,59,63,1,63,5,67,1,6,67,71,2,71,9,75,1,6,75,79,2,13,79,83,1,9,83,87,1,87,13,91,2,91,10,95,1,6,95,99,1,99,13,103,1,13,103,107,2,107,10,111,1,9,111,115,1,115,10,119,1,5,119,123,1,6,123,127,1,10,127,131,1,2,131,135,1,135,10,0,99,2,14,0,0];
var code = input.slice();

function executeInstruction(offset = 0) {
    if(code[offset] === 1) {
        code[code[offset + 3]] = code[code[offset + 1]] + code[code[offset + 2]];
        executeInstruction(offset + 4);
    } else if(code[offset] === 2) {
        code[code[offset + 3]] = code[code[offset + 1]] * code[code[offset + 2]];
        executeInstruction(offset + 4);
    } else if(code[offset] === 99) {
        return code[0];
    } else {
        console.log("unexpected value!")
    }
}

function findFirstSolution() {
    code = input.slice();
    code[1] = 12;
    code[2] = 2;
    executeInstruction();
    console.log(`response1: ${code[0]}`);
}

function findNounAndVerb(){
    var found = false;
    for (noun = 0; noun < 100; noun ++) {
        for (verb = 0; verb < 100; verb ++) {
            code = input.slice();
            code[1] = noun;
            code[2] = verb;
            executeInstruction(0);
            if (code[0] === 19690720) {
                found = true;
                console.log(`reponse2: ${noun * 100 + verb}`);
                break;
            }
        }
        if (found) {
            break;
        }
    }
}

findFirstSolution();
findNounAndVerb();
