const min = 137683;
const max = 596253;


function isGrowingArray(digits) {
    for (var i = 1; i < digits.length; i ++){
        if (digits[i-1] > digits[i]) return false;
    }
    return true;
}

function hasConsecutiveNumber(digits) {
    for (var i = 1; i < digits.length; i ++){
        if (digits[i-1] === digits[i]) return true;
    }
    return false;
}

function isGroupOf2(digits) {
    let groups = getGroups(digits);
    return groups.filter(function (n) { return n ===2;}).length > 0
}

function getGroups(array, groups = [], counter = 0) {
    if (array.length === 1) {
        if (counter > 0) {
            groups[groups.length] = ++ counter;
        }
        return groups;
    }
    else if (array[0] === array[1]) return getGroups(array.slice(1), groups, ++counter);
    else if(counter > 0) {
        groups[groups.length] = ++ counter;
        return getGroups(array.slice(1), groups, 0)
    } else return getGroups(array.slice(1), groups, 0);
}

function asDigits(num) {
    var digits = [];
    while (num >= 1) {
        digits = [(num % 10)].concat(digits);
        num = Math.floor(num/10);
    }
    return digits;
}

var part1 = 0;
var part2 = 0;

for (var i = min; i <= max; i++ ) {
    var digits = asDigits(i);
    if (isGrowingArray(digits) && hasConsecutiveNumber(digits) ) {
        part1++;
        if (isGroupOf2(digits)) {
            part2++;
        }
    }
}

console.log(part1);
console.log(part2);

