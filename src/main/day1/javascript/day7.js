var input = [3,8,1001,8,10,8,105,1,0,0,21,34,47,72,81,102,183,264,345,426,99999,3,9,102,5,9,9,1001,9,3,9,4,9,99,3,9,101,4,9,9,1002,9,3,9,4,9,99,3,9,102,3,9,9,101,2,9,9,102,5,9,9,1001,9,3,9,1002,9,4,9,4,9,99,3,9,101,5,9,9,4,9,99,3,9,101,3,9,9,1002,9,5,9,101,4,9,9,102,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,2,9,9,4,9,99,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,99];
// var input = [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0];
var a, b, c;

function executeInstruction(offset = 0, inputArray = [], out) {
    const[op, p1, p2, p3] = decodeInstruction(code[offset]);
    a = p1 === 1? code[offset + 1] : code[code[offset + 1]];
    b = p2 === 1? code[offset + 2] : code[code[offset + 2]];
    c = code[offset + 3];
    if(op === 1) {
        code[c] = a + b;
        return executeInstruction(offset + 4, inputArray, out);
    } else if(op === 2) {
        code[c] = a * b;
        return executeInstruction(offset + 4, inputArray, out);
    } else if(op === 3) {
        code[code[offset + 1]] = inputArray[0];
        return executeInstruction(offset + 2, inputArray.slice(1), out);
    } else if(op === 4) {
        return executeInstruction(offset + 2, inputArray, a);
    } else if(op === 5) {
        if (a) return executeInstruction(b, inputArray);
        else return executeInstruction(offset + 3, inputArray, out);
    } else if(op === 6) {
        if (a === 0) return executeInstruction(b, inputArray, out);
        else return executeInstruction(offset + 3, inputArray, out);
    } else if(op === 7) {
        if (a < b) code[c] = 1;
        else code[c] = 0;
        return executeInstruction(offset + 4, inputArray, out);
    } else if(op === 8) {
        if (a === b) code[c] = 1;
        else code[c] = 0;
        return executeInstruction(offset + 4, inputArray, out);
    } else if(op === 99) {
        return out;
    } else {
        console.log(`unexpected value ${op},${p1},${p2},${p3}  at offset ${offset}`);
    }
}

function decodeInstruction(num) {
    var op = num%100;
    var p1 = Math.floor(num/100)%10;
    var p2 = Math.floor(num/1000)%10;
    var p3 = Math.floor(num/10000)%10;
    return [op, p1, p2, p3];
}

function permutationsOf(phaseSettings, permBuilt = [], acc = []) {
    if (phaseSettings.length === 1) acc[acc.length] = permBuilt.concat(phaseSettings);
    else {
        for (phase of phaseSettings) {
            // permBuilt[permBuilt.length] = phase;
            permutationsOf(phaseSettings.filter(p => p !== phase), permBuilt.concat([phase]), acc);
        }
        // return acc;
    }
}

var permutations = [];
permutationsOf([0,1,2,3,4],[], permutations);

var bestPermutation = [];
var max = 0;
for (perm of permutations) {
    var inputNextPhase = 0;
    for (phase of perm) {
        var code = input.slice();
        inputNextPhase = executeInstruction(0, [phase, inputNextPhase]);
        console.log(inputNextPhase);
    }
    if (inputNextPhase > max) {
        max = inputNextPhase;
        bestPermutation = perm;
    }
}
console.log(max);
console.log(bestPermutation);
